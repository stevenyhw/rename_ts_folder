def rename_dir(dir_name: str, str_pattern: str, new_prefix: str) -> str:
  p = re.compile(str_pattern)
  files = list(os.listdir(dir_name))
  file_name = list(filter(lambda x: p.match(x), files))[0]
  new_dir = new_folder_name(new_prefix, file_name)
  os.rename(dir_name, new_dir)
  return "mv {} {}".format(dir_name, new_dir)